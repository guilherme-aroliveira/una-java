/*  Nome: Guilherme Augusto R. Oliveira
 *  Disciplina: Programação de Computadores
 *  Programa: Escrita com entrada de dados
 */

package pratica_07;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Problema_03 
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);

        //Declaração das variáveis
        String nome, sobrenome, dados;
        int idade, cont;
        StringTokenizer separator;

        File file = new File("/home/guilherme/Desktop/output.txt");

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {

            //Entrada de dados
            System.out.print("Digite o seu nome: ");
            nome = sc.nextLine();
            System.out.print("Digite o seu sobrenome: ");
            sobrenome = sc.nextLine();
            System.out.print("Digite a sua idade: ");
            idade = sc.nextInt();
            sc.close();

            //Separa a string
            separator = new StringTokenizer(nome + " " + sobrenome + " " + idade);

            cont = separator.countTokens();
            for(int i = 0; i < cont; i++)
            {
                dados = separator.nextToken();
                bw.append(dados); //Escreve no final do arquivo
            }
        }
        catch (IOException e) {
            System.out.println(e.getClass());
            System.out.println("Error: " + e.getMessage());
        }
    }
}
