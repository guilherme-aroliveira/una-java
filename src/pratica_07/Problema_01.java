/*  Nome: Guilherme Augusto R. Oliveira
 *  Disciplina: Programação de Computadores
 *  Programa: Escrita simples em arquivo
 */

package pratica_07;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Problema_01 
{
    public static void main(String[] args)
    {
        File file = new File("/home/guilherme/Desktop/output.txt");

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))) {

            bw.write("Olá, Mundo\n");
        }
        catch (IOException e) {
            System.out.println(e.getClass());
            System.out.println("Error: " + e.getMessage());
        }
    }
}
