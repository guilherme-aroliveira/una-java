/*  Nome: Guilherme Augusto R. Oliveira
 *  Disciplina: Programação de Computadores
 *  Programa: Escrita com entrada de dados heterogêneos
 */

package pratica_07;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Problema_05 
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);

        int cont;
        String dados;
        StringTokenizer separator;

        conta c = new conta();
        //Informe o path com o nome do arquivo
        File file = new File("/home/guilherme/Desktop/output.txt");

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {

            //Entrada de dados
            System.out.print("Informe o número da conta: ");
            c.numConta = sc.nextInt();
            System.out.print("Informe o número da agência: ");
            c.agencia = sc.nextInt();
            System.out.print("Informe o saldo da conta: ");
            c.saldo = sc.nextDouble();
            sc.close();

            //Separa a string
            separator = new StringTokenizer(c.numConta + "; " + c.agencia + "; " + c.saldo);

            cont = separator.countTokens();
            for(int j = 0; j < cont; j++)
            {
                dados = separator.nextToken();
                bw.append(dados); //Escreve no final do arquivo
                bw.newLine(); //quebra de linha
            }
        }
        catch (IOException e)
        {
            System.out.println(e.getClass());
            System.out.println("Error: " + e.getMessage());
        }
    }
}
