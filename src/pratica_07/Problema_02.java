/*  Nome: Guilherme Augusto R. Oliveira
 *  Disciplina: Programação de Computadores
 *  Programa: Escrita sequencial
 */

package pratica_07;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

public class Problema_02 
{
    public static void main(String[] args)
    {
        String[] lines = new String[] {"Good morning", "Good afternoon", "Good night"};
        String path = "/home/guilherme/Desktop/output.txt";

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path, true))) {
            for (String line : lines) {
                bw.write(line);
                bw.newLine();
            }
        }
        catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
