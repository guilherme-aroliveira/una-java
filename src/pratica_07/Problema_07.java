/*  Nome: Guilherme Augusto R. Oliveira
 *  Disciplina: Programação de Computadores
 *  Programa: Leitura senquencial
 */

package pratica_07;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Problema_07 
{
    public static void main(String[] args)
    {
         //Informe o path com o nome do arquivo
        String path = "/home/guilherme/Desktop/atividade.txt";

        //Abre o arquivo para leitura
        try(BufferedReader br = new BufferedReader(new FileReader(path)))
        {
            String line = br.readLine();

            while (line != null) {
                System.out.println(line);
                line = br.readLine();
            }
        }
        catch (IOException e)
        {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
